package com.mvvm.util

import android.content.Context
import java.io.BufferedReader
import java.io.InputStreamReader

fun readFileFromAssets(fileName: String, context: Context): String {
    val builder = StringBuilder()
    var reader: BufferedReader? = null
    try {
        reader = BufferedReader(
            InputStreamReader(context.resources.assets.open(fileName), "UTF-8")
        )
        var mLine: String?
        do {
            mLine = reader.readLine()
            if (mLine == null)
                break
            builder.append(mLine)
        } while (true)
    } catch (e: Exception) {
        //log the exception
    } finally {
        if (reader != null) {
            try {
                reader.close()
            } catch (e: Exception) {
                //log the exception
            }
        }
    }
    return builder.toString()
}
