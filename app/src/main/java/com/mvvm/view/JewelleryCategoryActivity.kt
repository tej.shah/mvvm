package com.mvvm.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders.of
import androidx.recyclerview.widget.LinearLayoutManager
import com.mvvm.R
import com.mvvm.viewmodel.CategoryViewModel
import kotlinx.android.synthetic.main.activity_jewellery_category.*

class JewelleryCategoryActivity : AppCompatActivity() {
    var viewModel: CategoryViewModel? = null
    val mCategoryListAdapter = CategoryListAdapter(arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jewellery_category)

        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory(application)
        ).get(CategoryViewModel::class.java)

        viewModel?.refresh()
        categoryList.apply {
            layoutManager = LinearLayoutManager(this@JewelleryCategoryActivity)
            adapter = mCategoryListAdapter
        }
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel?.jewelleryList?.observe(this, Observer {
            categoryList.visibility = View.VISIBLE
            listError.visibility = View.GONE
            mCategoryListAdapter.updateDogList(it)
        })

        viewModel?.loadError?.observe(this, Observer {
            if(it) {
                listError.visibility = View.VISIBLE
                categoryList.visibility = View.GONE
            }
        })

        viewModel?.loading?.observe(this, Observer {
            loadingView.visibility = if (it) View.VISIBLE else View.GONE
        })

    }
}