package com.mvvm.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.mvvm.R
import com.mvvm.viewmodel.DetailViewModel
import kotlinx.android.synthetic.main.activity_jewellery_detail.*

class JewelleryDetailActivity : AppCompatActivity() {
    var viewModel: DetailViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jewellery_detail)

        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory(application)
        ).get(DetailViewModel::class.java)

        viewModel?.refresh()

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel?.jewelleryModel?.observe(this, Observer {
            Glide.with(this).load(it.imageURL).into(imageDetail)
            DetailTitle.text = it.title.toString()
            DetailDescription.text = it.description.toString()
            DetailPrice.text = it.price.toString()
        })
    }
}