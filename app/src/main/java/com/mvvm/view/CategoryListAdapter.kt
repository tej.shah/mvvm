package com.mvvm.view

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mvvm.R
import com.mvvm.model.JewelleryCategory
import kotlinx.android.synthetic.main.item_jewellery.view.*

class CategoryListAdapter(val jewelleryList: ArrayList<JewelleryCategory>) :
    RecyclerView.Adapter<CategoryListAdapter.JewelleryViewHolder>() {

    fun updateDogList(newJewelleryList: List<JewelleryCategory>) {
        jewelleryList.clear()
        jewelleryList.addAll(newJewelleryList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JewelleryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_jewellery, parent, false)
        return JewelleryViewHolder(view)
    }

    override fun getItemCount() = jewelleryList.size

    override fun onBindViewHolder(holder: JewelleryViewHolder, position: Int) {
        holder.view.title.text = "${jewelleryList[position].title}"
        holder.view.description.text = "${jewelleryList[position].description}"
        holder.view.price.text = "Price: ${jewelleryList[position].price} Rs/-"
        Glide.with(holder.view.context).load(jewelleryList[position].imageURL).into(holder.view.imageView)
        holder.view.setOnClickListener {
            holder.view.context.startActivity(Intent(holder.view.context,JewelleryDetailActivity::class.java))
        }
    }

    class JewelleryViewHolder(var view: View) : RecyclerView.ViewHolder(view)
}