package com.mvvm.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface JewelleryDao {
    @Insert
    suspend fun insertAll(vararg category: JewelleryCategory): List<Long>

    @Query("SELECT * FROM JewelleryCategory")
    suspend fun getAllCategory(): List<JewelleryCategory>

    @Query("SELECT * FROM JewelleryCategory WHERE categoryID = :catID")
    suspend fun getCategoryByID(catID: Int): JewelleryCategory

    @Query("DELETE FROM JewelleryCategory")
    suspend fun deleteJewelleryCategory()
}