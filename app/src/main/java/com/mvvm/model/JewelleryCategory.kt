package com.mvvm.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class JewelleryCategory(
    val categoryID: Int? = 0,
    val title: String? = "",
    val description: String? = "",
    val price: String? = "",
    val discountedPrice: String? = "",
    val imageURL: String? = ""
) {
    @PrimaryKey(autoGenerate = true)
    var uuid: Int = 0
}