package com.mvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mvvm.model.JewelleryCategory

class DetailViewModel : ViewModel() {
    val jewelleryModel = MutableLiveData<JewelleryCategory>()

    fun refresh() {
        val first = JewelleryCategory(
            "Ring",
            "Pure Silver RING",
            "2500",
            "2000",
            "https://i.imgur.com/7ZrheHX.png"
        )
        jewelleryModel.value = first
    }

}