package com.mvvm.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mvvm.model.JewelleryCategory
import com.mvvm.util.readFileFromAssets
import java.lang.reflect.Type

class CategoryViewModel (application: Application) : AndroidViewModel(application) {
    val jewelleryList = MutableLiveData<List<JewelleryCategory>>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    fun refresh() {
      fetchResponseFromAssetsFile()
    }

    private fun fetchResponseFromAssetsFile() {
        var response = readFileFromAssets("api.json",getApplication())
        val listType: Type = object : TypeToken<List<JewelleryCategory?>?>() {}.getType()
        val list: List<JewelleryCategory> = Gson().fromJson(response, listType)

        jewelleryList.value = list
        loadError.value = false
        loading.value = false
    }

}